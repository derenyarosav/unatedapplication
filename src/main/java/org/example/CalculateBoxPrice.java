package org.example;

public class CalculateBoxPrice {
    public int calculateBoxPrice(Box box, int pricePerMeter) {
        int boxSize = box.getSide();
        int boxArea = 6 * boxSize * boxSize ; // площа поверхні куба в мм²
        double pricePerMilimeter = pricePerMeter / 1000d / 1000d;
        return (int) Math.ceil(boxArea * pricePerMilimeter);
    }

}
